/* student test program 4

   Message length test: check if message with length > MINIMSG_MAX_MSG_SIZE  is not sent

   USAGE: ./student-test-files-4 <port>

   where <port> is the minimsg port to use
*/

#include "minithread.h"
#include "minimsg.h"
#include "synch.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>


#define BUFFER_SIZE MINIMSG_MAX_MSG_SIZE 


miniport_t *listen_port;
miniport_t *send_port;

char text[MINIMSG_MAX_MSG_SIZE ];
int textlen = 14;

int
thread(int* arg) {
    char buffer[BUFFER_SIZE];
    int length = BUFFER_SIZE;
    miniport_t *from;
    network_address_t my_address;

    network_get_my_address(my_address);
    listen_port = miniport_create_unbound(0);
    send_port = miniport_create_bound(my_address, 0);
    int i;
	for(i = 0; i < MINIMSG_MAX_MSG_SIZE-1 ; i++)
		text[i] = 'a';
	text[i]='\0';
    minimsg_send(listen_port, send_port, text, textlen);
    minimsg_receive(listen_port, &from, buffer, &length);
    printf("%s", buffer);

    return 0;
}

int
main(int argc, char** argv) {
    short fromport;
    fromport = atoi(argv[1]);
    network_udp_ports(fromport,fromport); 
    textlen = strlen(text) + 1;
    minithread_system_initialize(thread, NULL);
    return -1;
}


