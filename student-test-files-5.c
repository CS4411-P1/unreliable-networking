/* student test program 5

   Port Scalability test: Check if all ports can be used properly. Also check if bound port fails if all ports are full
						  Also check if bound port wraps correctly

   USAGE: ./student-test-files-5 <port>

   where <port> is the minimsg port to use
*/

#include "minithread.h"
#include "minimsg.h"
#include "synch.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>


#define BUFFER_SIZE 256
#define UNBOUND_LAST 32767
#define BOUND_START 32768
#define BOUND_LAST 65535


miniport_t *listen_ports[UNBOUND_LAST+1];
miniport_t *send_ports[BOUND_LAST-BOUND_START+1];

char text[] = "Hello, world!\n";
int textlen = 14;

int
thread(int* arg) {
    char buffer[BUFFER_SIZE];
    int length = BUFFER_SIZE;
    miniport_t *from;
    network_address_t my_address;

    network_get_my_address(my_address);
    
    int i;
    
	for(i = 0; i <= UNBOUND_LAST; i++){
		listen_ports[i] = miniport_create_unbound(i);
		send_ports[i] = miniport_create_bound(my_address, i);
		printf("sending from port %d to receive port %d\n",i+BOUND_START,i);	
		minimsg_send(listen_ports[i], send_ports[i], text, textlen);
		minimsg_receive(listen_ports[i], &from, buffer, &length);
		printf("Port %d received message: %s from port %d\n",i,buffer,i+BOUND_START);
		miniport_destroy(send_ports[i]); //Comment this line for scale test; Uncomment this file for port creation test
	}
	
	//Check if creating a bound port now throws error
	miniport_t* new_port = miniport_create_bound(my_address, 0);
	if(new_port == NULL){
		printf("Cannot create any more bound ports\n");
	}
	
	miniport_t* new_port2 = miniport_create_bound(my_address, 0);
	if(new_port2 == NULL){
		printf("Cannot create any more bound ports\n");
	}

    return 0;
}

int
main(int argc, char** argv) {
    short fromport;
    fromport = atoi(argv[1]);
    network_udp_ports(fromport,fromport); 
    textlen = strlen(text) + 1;
    minithread_system_initialize(thread, NULL);
    return -1;
}


